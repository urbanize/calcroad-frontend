# NPM compilation
FROM node AS npm

ARG MODE

# Copy our source code into the container
COPY . /home

# run webpack and the vue-loader
# copy the built app to our served directory
RUN cd /home && \ 
    npm install && \
    npm run build:$MODE && \
    cp -r /home/dist/* /tmp

RUN ls -l /tmp

# Create the container from the alpine linux image
FROM nginx

# Create the directories we will need
RUN mkdir -p /var/log/nginx && \
    mkdir -p /var/www/html

# Copy the respective nginx configuration files
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/default.conf /etc/nginx/conf.d/default.conf

# Copy the builded app from npm
COPY --from=npm /tmp /var/www/html

# start nginx and keep the process from backgrounding and the container from quitting
CMD ["nginx", "-g", "daemon off;"]
