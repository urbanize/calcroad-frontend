build-container:
	docker build -t calcroad_frontend .

run-container:
	docker run -p 8080:80 calcroad_frontend

test-container:
	make build-container
	make run-container

run-dev:
	npm run serve
