# calcroad

CalcRoad est un outil pour simulateur de trafic routier.

## Configuration du Projet

### Paramétrer les requêtes vers l'API

1. Voir le [git](https://gitlab.com/urbanize/backend) pour configurer l'API.

2. Modifier les fichiers de configurations en fonction du type d'environnement.
   - [`.env`](.env) permet de configurer les variables pour un environnement de développement.
   - [`.env.integration`](.env.integration) permet de configurer les variables pour un environnement de pré-production.
   - [`.env.production`](.env.production) permet de configurer les variables pour un environnement de production.

|Variable|Description|valeur|
|:-|:-|:-:|
|NODE_ENV| Utilisé pour forcer node.js de s'exécuter dans un mode particulier. | `development`|
|VUE_APP_TITLE| Permet de changer le titre de l'application. | *Calc Road*|
|VUE_APP_API_URL| Permet de configurer l'endpoint de l'API. |*selon le déploiement*



## Contribution

```python
npm install # Installer les dépendances.
npm run serve # Lancer l'application en mode développeur.
npm run build # Exporter l'application en mode pré-production.
npm run build:production # Exporter l'application en mode production.
```

### Documentations externes

See [Configuration Reference](https://cli.vuejs.org/config/).
