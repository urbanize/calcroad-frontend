import { GeometryRoadsBase } from "../base";
import { PolylineOptions, LatLngExpression } from "leaflet";

export const RoadOptions: PolylineOptions = {
  smoothFactor: 1,
  color: "#4CAF50",
  stroke: true,
  noClip: false,
  weight: 3,
  opacity: 1.0,
  lineCap: "round",
  lineJoin: "round",
  fill: false,
  fillColor: "pink",
  fillOpacity: 0.2
};

export const zone110Options: PolylineOptions = {
  smoothFactor: 1,
  color: "#4527A0",
  stroke: true,
  noClip: false,
  weight: 4,
  opacity: 1.0,
  lineCap: "round",
  lineJoin: "round",
  fill: false
};

export const zone90Options: PolylineOptions = {
  smoothFactor: 1,
  color: "#304FFE",
  stroke: true,
  noClip: false,
  weight: 3.7,
  opacity: 1.0,
  lineCap: "round",
  lineJoin: "round",
  fill: false
};

export const zone70Options: PolylineOptions = {
  smoothFactor: 1,
  color: "#2196F3",
  stroke: true,
  noClip: false,
  weight: 3.3,
  opacity: 1.0,
  lineCap: "round",
  lineJoin: "round",
  fill: false
};

export const zone30Options: PolylineOptions = {
  smoothFactor: 1,
  color: "#FF9800",
  stroke: true,
  noClip: false,
  weight: 2.5,
  opacity: 1.0,
  lineCap: "round",
  lineJoin: "round",
  fill: false,
  fillColor: "pink",
  fillOpacity: 0.2
};

export const CongestionOptions: PolylineOptions = {
  smoothFactor: 1,
  color: "#F44330",
  stroke: true,
  noClip: false,
  weight: 5.5,
  opacity: 1.0,
  lineCap: "round",
  lineJoin: "round",
  fill: false
};

export const HighWayOptions: PolylineOptions = {
  smoothFactor: 1,
  color: "#9C27B0",
  stroke: true,
  noClip: false,
  weight: 5,
  opacity: 1.0,
  lineCap: "round",
  lineJoin: "round",
  fill: false
};

export class GeometryRoad extends GeometryRoadsBase {
  constructor(
    id: number,
    name: string,
    coords: LatLngExpression[],
    max_speed = "50",
    options = RoadOptions
  ) {
    super(id, name, max_speed, coords, options);
  }
}
