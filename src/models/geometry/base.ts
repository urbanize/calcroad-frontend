import {
  Polyline,
  LatLngExpression,
  PolylineOptions,
  Marker,
  LatLng,
  MarkerOptions
} from "leaflet";

abstract class GeometryBase {
  abstract id: number;
  abstract name: string;
  abstract get coordinates(): number[] | number[][];
}

const RoadBaseOptions: PolylineOptions = {};

export class GeometryRoadsBase extends Polyline implements GeometryBase {
  name: string;
  id: number;
  max_speed: string;

  constructor(
    id: number,
    name: string,
    max_speed: string,
    coords: LatLngExpression[],
    options: PolylineOptions = RoadBaseOptions
  ) {
    super(coords, Object.assign(RoadBaseOptions, options));
    this.name = name;
    this.id = id;
    this.max_speed = max_speed;
  }

  get coordinates(): number[][] {
    let latlngs = this.getLatLngs();

    // @ts-ignore
    return latlngs.map((latlng: LatLng) => {
      return [latlng.lat, latlng.lng];
    });
  }
}

const PlaceBaseOptions: MarkerOptions = {};
export const HomeOptions: MarkerOptions = {
  keyboard: true,
  title: "Habitation",
  opacity: 1.0,
  pane: "markerPane",
  // @ts-ignore
  icon: L.icon({
    iconUrl:
      "https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png",
    shadowUrl:
      "https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  })
};

export const WorkOptions: MarkerOptions = {
  keyboard: true,
  title: "Lieu de travail",
  opacity: 1.0,
  pane: "markerPane",
  // @ts-ignore
  icon: L.icon({
    iconUrl:
      "https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png",
    shadowUrl:
      "https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  })
};

export class GeometryJourneysBase implements GeometryBase {
  name: string;
  id: number;
  start_time: number;
  max_vehicules: number;
  start: Marker;
  end: Marker;

  constructor(
    id: number,
    name: string,
    start_point: LatLngExpression,
    end_point: LatLngExpression,
    start_time: number,
    max_vehicules: number = 1
  ) {
    this.start = new Marker(
      start_point,
      Object.assign(PlaceBaseOptions, HomeOptions)
    );
    this.end = new Marker(
      end_point,
      Object.assign(PlaceBaseOptions, WorkOptions)
    );

    this.start_time = start_time;
    this.name = name;
    this.id = id;

    this.max_vehicules = max_vehicules;
  }

  get coordinates(): number[][] {
    let latlng = this.start.getLatLng();
    let start_latlng: number[] = [latlng.lat, latlng.lng];

    latlng = this.end.getLatLng();
    let end_latlng: number[] = [latlng.lat, latlng.lng];

    return [start_latlng, end_latlng];
  }
}

const EventBaseOptions: MarkerOptions = {};

export class GeometryEventsBase extends Marker implements GeometryBase {
  name: string;
  id: number;

  constructor(
    id: number,
    name: string,
    coords: LatLngExpression,
    options: MarkerOptions = EventBaseOptions
  ) {
    super(coords, Object.assign(EventBaseOptions, options));
    this.name = name;
    this.id = id;
  }

  get coordinates(): number[] {
    let latlng = this.getLatLng();
    return [latlng.lat, latlng.lng];
  }
}
