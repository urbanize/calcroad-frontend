import { MarkerOptions, LatLngExpression } from "leaflet";
import { GeometryEventsBase } from "../base";

const EventOptions: MarkerOptions = {
  keyboard: true,
  title: "Accident",
  opacity: 1.0,
  pane: "markerPane"
};

const ClosedRoadOptions: MarkerOptions = {
  keyboard: true,
  title: "Route fermee",
  opacity: 1.0,
  pane: "markerPane"
};

const RoadWorkOptions: MarkerOptions = {
  keyboard: true,
  title: "Travaux",
  opacity: 1.0,
  pane: "markerPane"
};

const AccidentOptions: MarkerOptions = {
  keyboard: true,
  title: "Accident",
  opacity: 1.0,
  pane: "markerPane"
};

const StoppedCarOptions: MarkerOptions = {
  keyboard: true,
  title: "Voiture a l arret",
  opacity: 1.0,
  pane: "markerPane"
};

const RainOptions: MarkerOptions = {
  keyboard: true,
  title: "Pluie",
  opacity: 1.0,
  pane: "markerPane"
};

const SnowOptions: MarkerOptions = {
  keyboard: true,
  title: "Neige",
  opacity: 1.0,
  pane: "markerPane"
};

const TrafficLightOptions: MarkerOptions = {
  keyboard: true,
  title: "Feu tricolor",
  opacity: 1.0,
  pane: "markerPane"
};

//alt a ajouter
// ClosedRoadOptions
// RoadWorkOptions
// AccidentOptions
// StoppedCarOptions
// RainOptions
// HabitationOptions

export class GeometryEvent extends GeometryEventsBase {
  constructor(id: number, name: string, coords: LatLngExpression) {
    super(id, name, coords, EventOptions);
  }
}

export class GeometryClosedRoad extends GeometryEventsBase {
  constructor(id: number, name: string, coords: LatLngExpression) {
    super(id, name, coords, ClosedRoadOptions);
  }
}

export class GeometryWorkRoad extends GeometryEventsBase {
  constructor(id: number, name: string, coords: LatLngExpression) {
    super(id, name, coords, RoadWorkOptions);
  }
}

export class GeometryAccident extends GeometryEventsBase {
  constructor(id: number, name: string, coords: LatLngExpression) {
    super(id, name, coords, AccidentOptions);
  }
}

export class GeometryStoppedCar extends GeometryEventsBase {
  constructor(id: number, name: string, coords: LatLngExpression) {
    super(id, name, coords, StoppedCarOptions);
  }
}

export class GeometryRain extends GeometryEventsBase {
  constructor(id: number, name: string, coords: LatLngExpression) {
    super(id, name, coords, RainOptions);
  }
}

export class GeometrySnow extends GeometryEventsBase {
  constructor(id: number, name: string, coords: LatLngExpression) {
    super(id, name, coords, SnowOptions);
  }
}

export class GeometryTrafficLight extends GeometryEventsBase {
  constructor(id: number, name: string, coords: LatLngExpression) {
    super(id, name, coords, TrafficLightOptions);
  }
}
