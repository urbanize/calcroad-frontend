export interface Map {
  id?: string;
  title: string;
  public: boolean;
  center: number[];
}
