export interface UserLogin {
  email: string;
  password?: string;
}

export interface UserRegister {
  email: string;
  first_name: string;
  last_name: string;
  password: string;
}

export interface UserLoginSuccess {
  data: {
    access_token: string;
    refresh_token: string;
    expires: number;
  };
}
export interface UserLoginError {
  response:
    | {
        status: number;
      }
    | undefined;
}
