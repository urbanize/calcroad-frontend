import { GetterTree } from "vuex";
import { RootState } from "../types";
import { AlertState } from "./types";

const getters: GetterTree<AlertState, RootState> = {
  alert(state): AlertState {
    return {
      type: state.type,
      message: state.message,
      icon: state.icon
    };
  }
};

export default getters;
