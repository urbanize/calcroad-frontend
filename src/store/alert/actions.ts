import { ActionTree } from "vuex";
import { RootState } from "../types";
import { AlertState, SUCCESS, ERROR, CLEAR, WARNING } from "./types";

const actions: ActionTree<AlertState, RootState> = {
  success: ({ commit }, message) => {
    commit(SUCCESS, message);
  },
  warning: ({ commit }, message) => {
    commit(WARNING, message);
  },
  error: ({ commit }, message) => {
    commit(ERROR, message);
  },
  clear: ({ commit }, message) => {
    commit(CLEAR);
  }
};

export default actions;
