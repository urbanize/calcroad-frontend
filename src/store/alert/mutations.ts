import { MutationTree } from "vuex";
import { AlertState, SUCCESS, ERROR, CLEAR, WARNING } from "./types";

const mutations: MutationTree<AlertState> = {
  [SUCCESS]: (state, message) => {
    state.type = "success";
    state.message = message;
    state.icon = "mdi-check-circle";
  },
  [WARNING]: (state, message) => {
    state.type = "warning";
    state.message = message;
    state.icon = "mdi-alert-circle";
  },
  [ERROR]: (state, message) => {
    state.type = "error";
    state.message = message;
    state.icon = "mdi-close-circle";
  },
  [CLEAR]: state => {
    state.type = "";
    state.message = "";
    state.icon = "";
  }
};

export default mutations;
