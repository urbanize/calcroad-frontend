import { Module } from "vuex";
import { RootState } from "../types";
import { AlertState } from "./types";

import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";

const state: AlertState = {
  type: "",
  message: "",
  icon: ""
};

const alert: Module<AlertState, RootState> = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};

export default alert;
