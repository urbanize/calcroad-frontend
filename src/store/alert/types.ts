export interface AlertState {
  type: string;
  message: string;
  icon: string;
}

export const SUCCESS = "SUCCESS";
export const WARNING = "WARNING";
export const ERROR = "ERROR";
export const CLEAR = "CLEAR";
