import { MutationTree } from "vuex";
import {
  SimulationState,
  PLAYER,
  UPDATE_START,
  UPDATE_END,
  UP_SPEED,
  UPDATE_CURRENT,
  INITIALIZE,
  DOWN_SPEED
} from "./types";

const mutations: MutationTree<SimulationState> = {
  [INITIALIZE]: state => {
    state.player = false;
    state.start = 28800; // 08h00m00s
    state.end = 75600; // 21h00m00s
    state.speed = 1;

    // Buffer
    state.next_buffer = 28800;
    state.buffer_len = 50;
    state.reload = false;
    state.time = 0;

    // setInterval
    clearInterval(state.timer);
  },
  [PLAYER]: (state, player: Boolean) => {
    state.player = player;
  },
  [UPDATE_CURRENT]: (state, time: number) => {
    state.time = time;
  },
  [UPDATE_START]: (state, time: number) => {
    if (state.player) state.time = time;
    else {
      state.start = state.start > time ? time : state.start;
      state.time = state.start;
    }
  },
  [UPDATE_END]: (state, time: number) => {
    state.end = time;
  },

  [UP_SPEED]: state => {
    state.speed = state.speed <= 64 ? state.speed * 2 : 128;
  },
  [DOWN_SPEED]: state => {
    state.speed = state.speed > 1 ? state.speed / 2 : 1;
  }
};

export default mutations;
