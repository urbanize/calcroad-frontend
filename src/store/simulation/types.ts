import { GeometryCollection } from "geojson";

export interface SimulationState {
  // SimulationPlayer
  disabled: Boolean;
  player: Boolean;
  start: number;
  end: number;
  speed: number;

  // Buffer
  next_buffer: number;
  buffer_len: number;
  reload: Boolean;
  time: number;

  // setInterval
  timer: number;
}

export const PLAYER = "PLAYER";
export const UPDATE_CURRENT = "UPDATE_CURRENT";
export const INITIALIZE = "INITIALIZE";
export const DELETE_BUFFER = "DELETE_BUFFER";
export const UPDATE_START = "UPDATE_START";
export const UPDATE_END = "UPDATE_END";
export const UP_SPEED = "UP_SPEED";
export const DOWN_SPEED = "DOWN_SPEED";
