import { GetterTree } from "vuex";
import { RootState } from "../types";
import { SimulationState } from "./types";

const getters: GetterTree<SimulationState, RootState> = {
  disabled: state => state.disabled,
  isPlaying: state => state.player,

  currentTime: state => state.time,
  startTime: state => state.start,
  endTime: state => state.end,

  ////
  speed: state => state.speed,
  reload: state => state.reload,
  buffer_len: state => state.buffer_len,
  next_buffer: state => state.next_buffer
};

export default getters;
