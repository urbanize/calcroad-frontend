import { Module } from "vuex";
import { RootState } from "../types";
import { SimulationState } from "./types";

import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";

const state: SimulationState = {
  disabled: false,
  player: false,
  start: 28800, // 08h00m00s
  end: 75600, // 21h00m00s
  speed: 1,

  // Buffer
  next_buffer: 28800,
  buffer_len: 50,
  reload: false,
  time: 0,

  // setInterval
  timer: 0
};

const simulation: Module<SimulationState, RootState> = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};

export default simulation;
