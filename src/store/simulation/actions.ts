import { ActionTree } from "vuex";
import { RootState } from "../types";
import {
  SimulationState,
  PLAYER,
  UPDATE_START,
  UPDATE_END,
  UP_SPEED,
  DOWN_SPEED,
  UPDATE_CURRENT,
  INITIALIZE
} from "./types";

import SimulationService from "../../services/simulation";

const BUFFER_LENGTH = 30;

//////////////// MAIN Handler /////////////////////
const HandlerSimulation = (state: SimulationState) => {
  // check if the player is played
  if (state.player === true) {
    // if the count buffer for each map are less than 5.
    if (state.time === state.next_buffer && !state.reload) {
      state.reload = true;
      state.time += 1;
    } else {
      state.time += 1;
    }

    if (state.time === state.end) {
      //
      state.time = state.start;
      //clearInterval(state.timer);
    }
  }
};

const actions: ActionTree<SimulationState, RootState> = {
  initialize: ({ state }) => {
    clearInterval(state.timer);

    state.buffer_len = BUFFER_LENGTH * state.speed;
    state.next_buffer = state.time + BUFFER_LENGTH - 10;
    state.reload = true;

    state.timer = setInterval(HandlerSimulation, 1000 / state.speed, state);
  },

  addMap: ({ commit, dispatch }, map_id: string) => {
    SimulationService.run_simulation(map_id)
      .then((resp: any) => {
        if (resp.data.code === 0)
          dispatch("alert/success", "La simulation est prête !", {
            root: true
          });
      })
      .catch((err: any) => {
        dispatch("alert/error", err, { root: true });
        SimulationService.delete_simulation(map_id);
      });
  },
  delMap: ({ commit }) => {
    commit(INITIALIZE);
  },

  // about the simulationPlayer
  switchPlayer: ({ commit }, player: Boolean) => {
    commit(PLAYER, player);
  },
  update_start: ({ commit }, time: number) => {
    commit(UPDATE_START, time);
  },
  update_end: ({ commit }, time: number) => {
    commit(UPDATE_END, time);
  },
  update_current: ({ commit }, time: number) => {
    commit(UPDATE_CURRENT, time);
  },
  upSpeed: ({ commit, dispatch }) => {
    commit(UP_SPEED);
    dispatch("initialize");
  },
  downSpeed: ({ commit, dispatch }) => {
    commit(DOWN_SPEED);
    dispatch("initialize");
  }
};

export default actions;
