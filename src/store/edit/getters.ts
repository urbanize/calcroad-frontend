import { GetterTree } from "vuex";
import { RootState } from "../types";
import { EditState } from "./types";

const getters: GetterTree<EditState, RootState> = {
  map: state => state.mapObject,
  // @ts-ignore
  roads: state => state.roads.layers,
  // @ts-ignore
  journeys: state => state.journeys.layers,
  // @ts-ignore
  events: state => state.events.layers
};

export default getters;
