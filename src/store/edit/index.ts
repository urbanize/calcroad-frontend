import { Module } from "vuex";
import { RootState } from "../types";
import { EditState } from "./types";

import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";
import { LayerGroup } from "leaflet";

const state: EditState = {
  mapObject: undefined,
  roads: new LayerGroup(),
  journeys: new LayerGroup(),
  events: new LayerGroup()
};

const user: Module<EditState, RootState> = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};

export default user;
