import { ActionTree } from "vuex";
import { RootState } from "../types";
import {
  EditState,
  ADD_ROAD,
  ADD_JOURNEY,
  DEL_ROAD,
  DEL_JOURNEY,
  DEL_EVENT,
  ADD_EVENT
} from "./types";
import { Map } from "leaflet";
import roadsService from "../../services/roads";
import journeysService from "../../services/journeys";
import eventsService from "../../services/events";
import * as Geometry from "@/models/geometry";
import router from "@/router";
import Vue from "vue";

export const createRoad = (
  map_id: string,
  road: any,
  listener: Boolean = true
) => {
  let _road: Geometry.GeometryRoad;
  if (road.max_speed >= 50 && road.max_speed < 70) {
    _road = new Geometry.GeometryRoad(
      road.id,
      road.name,
      road.coordinates,
      road.max_speed,
      Geometry.RoadOptions
    );
  } else if (road.max_speed >= 130) {
    _road = new Geometry.GeometryRoad(
      road.id,
      road.name,
      road.coordinates,
      road.max_speed,
      Geometry.HighWayOptions
    );
  } else if (road.max_speed >= 110 && road.max_speed < 130) {
    _road = new Geometry.GeometryRoad(
      road.id,
      road.name,
      road.coordinates,
      road.max_speed,
      Geometry.zone110Options
    );
  } else if (road.max_speed >= 80 && road.max_speed < 110) {
    _road = new Geometry.GeometryRoad(
      road.id,
      road.name,
      road.coordinates,
      road.max_speed,
      Geometry.zone90Options
    );
  } else if (road.max_speed >= 70 && road.max_speed < 80) {
    _road = new Geometry.GeometryRoad(
      road.id,
      road.name,
      road.coordinates,
      road.max_speed,
      Geometry.zone70Options
    );
  } else if (road.max_speed === 15) {
    _road = new Geometry.GeometryRoad(
      road.id,
      road.name,
      road.coordinates,
      road.max_speed,
      Geometry.CongestionOptions
    );
  } else if (road.max_speed <= 30) {
    _road = new Geometry.GeometryRoad(
      road.id,
      road.name,
      road.coordinates,
      road.max_speed,
      Geometry.zone30Options
    );
  } else {
    _road = new Geometry.GeometryRoad(
      road.id,
      road.name,
      road.coordinates,
      road.max_speed,
      Geometry.RoadOptions
    );
  }
  if (listener)
    _road.on("click", () => {
      // `/edit/${map_id}/roads/${_road.id}`
      router.push({
        name: "edit-roads-detail",
        params: {
          map: map_id,
          id: `${_road.id}`
        }
      });
    });
  return _road;
};

export const createJourney = (
  map_id: string,
  journey: any,
  listener: Boolean = true
) => {
  let _journey: Geometry.GeometryJourneysBase;
  _journey = new Geometry.GeometryJourneysBase(
    journey.id,
    journey.name,
    journey.start_point,
    journey.end_point,
    journey.start_time,
    journey.max_vehicules
  );
  if (listener) {
    _journey.start.on("click", () => {
      // `/edit/${map_id}/journeys/${journey.id}`
      router.push({
        name: "edit-journeys-detail",
        params: {
          map: map_id,
          id: `${_journey.id}`
        }
      });
    });
    _journey.end.on("click", () => {
      // `/edit/${map_id}/journeys/${journey.id}`
      router.push({
        name: "edit-journeys-detail",
        params: {
          map: map_id,
          id: `${_journey.id}`
        }
      });
    });
  }

  return _journey;
};

const createEvent = (map_id: string, event: any) => {
  let _event: Geometry.GeometryEvent;
  _event = new Geometry.GeometryEvent(event.id, event.name, event.coords);
  _event.on("click", () => {
    //`/edit/${map_id}/events/${_event.id}`
    // `/edit/${map_id}/journeys/${journey.id}`
    router.push({
      name: "edit-events-detail",
      params: {
        map: map_id,
        id: `${_event.id}`
      }
    });
  });
  return _event;
};

const actions: ActionTree<EditState, RootState> = {
  initialize: ({ state }, mapObject: Map) => {
    state.roads.addTo(mapObject);
    state.journeys.addTo(mapObject);
    state.events.addTo(mapObject);

    Vue.set(state.roads, "layers", []);
    Vue.set(state.journeys, "layers", []);
    Vue.set(state.events, "layers", []);
  },
  unload: ({ state }) => {
    state.roads.clearLayers();
    state.journeys.clearLayers();
    state.events.clearLayers();
  },
  fetch_data: ({ commit }, map_id: string) => {
    // TODO: types and find a way to get the Klass of the object
    roadsService
      .get_roads(map_id)
      .then((resp: any) => {
        resp.data.forEach((road: any) => {
          commit(ADD_ROAD, createRoad(map_id, road));
        });
      })
      .catch((err: any) => {});

    journeysService
      .get_journeys(map_id)
      .then((resp: any) => {
        resp.data.forEach((journey: any) => {
          commit(ADD_JOURNEY, createJourney(map_id, journey));
        });
      })
      .catch((err: any) => {});

    eventsService
      .get_events(map_id)
      .then((resp: any) => {
        resp.data.forEach((event: any) => {
          commit(ADD_EVENT, createEvent(map_id, event));
        });
      })
      .catch((err: any) => {});
  },

  /**
   * @param type: If >= 0, it is the max_speed roads param;
   *              If == -1, it is a Journey;
   *              If < -1, it is a specific event;
   */
  create: ({ dispatch }, { map_id, type, coords }) => {
    if (type >= 0)
      dispatch("edit/add_road", { map_id, type, coords }, { root: true });

    if (type === -1)
      dispatch("edit/add_journey", { map_id, coords }, { root: true });

    // TODO: events
    // dispatch("edit/add_event", { map_id, Klass, coords }, { root: true });
  },
  add_road: ({ commit }, { map_id, type, coords }) => {
    roadsService.create_road(map_id, coords, type).then((resp: any) => {
      commit(ADD_ROAD, createRoad(map_id, resp.data));
    });
  },
  add_journey: ({ commit }, { map_id, coords }) => {
    journeysService.create_journey(map_id, coords).then((resp: any) => {
      commit(ADD_JOURNEY, createJourney(map_id, resp.data));
    });
  },
  add_event: ({ commit }, { map_id, Klass, coords }) => {
    eventsService.create_event(map_id, coords).then((resp: any) => {
      commit(ADD_EVENT, createEvent(map_id, resp.data));
    });
  },

  remove: ({ dispatch }, { map_id, instance }) => {
    if (instance instanceof Geometry.GeometryRoadsBase)
      dispatch("edit/del_road", { map_id, road: instance }, { root: true });

    if (instance instanceof Geometry.GeometryJourneysBase)
      dispatch(
        "edit/del_journey",
        { map_id, journey: instance },
        { root: true }
      );

    if (instance instanceof Geometry.GeometryEventsBase)
      dispatch("edit/del_event", { map_id, event: instance }, { root: true });
  },
  del_road: ({ commit }, { map_id, road }) => {
    roadsService
      .remove_road(map_id, road.id)
      .then((resp: any) => {
        commit(DEL_ROAD, road);
      })
      .catch((err: any) => {});
  },
  del_journey: ({ commit }, { map_id, journey }) => {
    journeysService
      .remove_journey(map_id, journey.id)
      .then((resp: any) => {
        commit(DEL_JOURNEY, journey);
      })
      .catch((err: any) => {});
  },
  del_event: ({ commit }, { map_id, event }) => {
    eventsService
      .remove_event(map_id, event.id)
      .then((resp: any) => {
        commit(DEL_EVENT, event);
      })
      .catch((err: any) => {});
  },

  update_road: ({ commit }, { map_id, road }) => {
    roadsService
      .update_road(map_id, road.id, road.name, road.coordinates, road.max_speed)
      .then((resp: any) => {
        commit(DEL_ROAD, road);
        commit(ADD_ROAD, createRoad(map_id, resp.data));
      });
  }
};

export default actions;
