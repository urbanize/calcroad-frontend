import Vue from "vue";
import { MutationTree } from "vuex";
import {
  EditState,
  ADD_ROAD,
  ADD_JOURNEY,
  ADD_EVENT,
  DEL_ROAD,
  DEL_JOURNEY,
  DEL_EVENT
} from "./types";
import {
  GeometryRoadsBase,
  GeometryJourneysBase,
  GeometryEventsBase
} from "@/models/geometry";

const mutations: MutationTree<EditState> = {
  [ADD_ROAD]: (state, road: GeometryRoadsBase) => {
    state.roads.addLayer(road);
    // hack to force updates of `state.roads` on getters
    Vue.set(state.roads, "layers", state.roads.getLayers());
  },
  [ADD_JOURNEY]: (state, journey: GeometryJourneysBase) => {
    state.journeys.addLayer(journey.start);
    state.journeys.addLayer(journey.end);
    // TODO: trouver une solution pour afficher correctement les journeys

    // @ts-ignore
    // eslint-disable-next-line prettier/prettier
    let old = state.journeys.layers ? state.journeys.layers : [];
    old.push(journey);
    Vue.set(state.journeys, "layers", old);
  },
  [ADD_EVENT]: (state, event: GeometryEventsBase) => {
    state.events.addLayer(event);
    Vue.set(state.events, "layers", state.events.getLayers());
  },
  [DEL_ROAD]: (state, road: GeometryRoadsBase) => {
    state.roads.removeLayer(road);
    Vue.set(state.roads, "layers", state.roads.getLayers());
  },
  [DEL_JOURNEY]: (state, journey: GeometryJourneysBase) => {
    state.journeys.removeLayer(journey.start);
    state.journeys.removeLayer(journey.end);

    // @ts-ignore
    let old: GeometryJourneysBase[] = state.journeys.layers;
    let nw = old.filter(f => f !== journey);
    Vue.set(state.journeys, "layers", nw);
  },
  [DEL_EVENT]: (state, event: GeometryEventsBase) => {
    state.events.removeLayer(event);
    Vue.set(state.events, "layers", state.events.getLayers());
  }
};

export default mutations;
