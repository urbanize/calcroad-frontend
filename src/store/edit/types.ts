import { Map, LayerGroup } from "leaflet";
import {
  GeometryRoadsBase,
  GeometryJourneysBase,
  GeometryEventsBase
} from "@/models/geometry";

export interface EditState {
  mapObject: Map | undefined;
  roads: LayerGroup<GeometryRoadsBase>;
  journeys: LayerGroup<GeometryJourneysBase>;
  events: LayerGroup<GeometryEventsBase>;
}

export const ADD_ROAD = "ADD_ROAD";
export const ADD_JOURNEY = "ADD_JOURNEY";
export const ADD_EVENT = "ADD_EVENT";
export const DEL_ROAD = "DEL_ROAD";
export const DEL_JOURNEY = "DEL_JOURNEY";
export const DEL_EVENT = "DEL_EVENT";
