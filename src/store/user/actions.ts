import router from "@/router";
import userService from "@/services/user";
import {
  UserLogin,
  UserLoginSuccess,
  UserLoginError,
  UserRegister
} from "@/models/user";

import { ActionTree } from "vuex";
import { RootState } from "../types";
import { UserState, LOGIN, LOGOUT } from "./types";

const actions: ActionTree<UserState, RootState> = {
  login: ({ commit, dispatch }, user: UserLogin) => {
    return userService
      .login(user)
      .then((resp: UserLoginSuccess) => {
        commit(LOGIN, resp.data.access_token);

        dispatch("alert/success", "Connection réussie.", { root: true });
        router.push({
          path: `${router.currentRoute.query.redirect_url || "/edit"}`
        });
      })
      .catch((err: UserLoginError) => {
        const message =
          err.response !== undefined && err.response.status === 401
            ? "Adresse email et/ou mot de passe non valide."
            : err;
        commit(LOGOUT);
        dispatch("alert/error", message, { root: true });
      });
  },
  refreshToken: ({ commit, dispatch }) => {
    // TODO: do something when the refresh token will be set.
    userService.check_validity("").then((resp: any) => {
      commit(LOGIN, localStorage.getItem("accessToken"));
      dispatch("alert/success", "Connection réussie.", { root: true });
      router.push({
        path: `${router.currentRoute.query.redirect_url || "/edit"}`
      });
    });
  },
  logout: ({ commit }) => {
    commit(LOGOUT);
    userService.logout();
    router.push({ name: "login" });
  },
  register: ({ dispatch }, user: UserRegister) => {
    userService
      .register(user)
      .then((resp: any) => {
        router.push({ name: "login" });
        dispatch("alert/success", "Enregistrement réussi.", { root: true });
        dispatch("login", { email: user.email, password: user.password });
      })
      .catch((err: any) => {
        router.push({ name: "register" });
        dispatch("alert/error", err, { root: true });
      });
  },
  change_password: ({ dispatch }, { token, password, verify }) => {
    userService
      .change_password(token, password, verify)
      .then((resp: any) => {
        router.push({ name: "login" });
        dispatch("alert/success", "Mot de passe changé.", { root: true });
      })
      .catch((err: any) => {
        router.push({ name: "change_password" });
        dispatch("alert/error", err, { root: true });
      });
  }
};

export default actions;
