import { MutationTree } from "vuex";
import { UserState, LOGIN, LOGOUT } from "./types";

const mutations: MutationTree<UserState> = {
  [LOGIN]: (state, token) => {
    state.token = token;
    state.authenticated = true;
    localStorage.setItem("accessToken", token);
  },
  [LOGOUT]: state => {
    state.token = "";
    state.authenticated = false;
    localStorage.removeItem("accessToken");
  }
};

export default mutations;
