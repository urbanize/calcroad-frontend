export interface UserState {
  token: string;
  authenticated: boolean;
}

export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
