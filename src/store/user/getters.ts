import { GetterTree } from "vuex";
import { RootState } from "../types";
import { UserState } from "./types";

const getters: GetterTree<UserState, RootState> = {
  isAuthenticated: state => state.authenticated
};

export default getters;
