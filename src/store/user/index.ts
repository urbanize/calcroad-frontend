import { Module } from "vuex";
import { RootState } from "../types";
import { UserState } from "./types";

import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";

const state: UserState = {
  token: localStorage.getItem("accessToken") || "",
  authenticated: false
};

const user: Module<UserState, RootState> = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};

export default user;
