import Vue from "vue";
import Vuex, { StoreOptions } from "vuex";

import { RootState } from "./types";
import simulation from "./simulation";
import alert from "./alert";
import user from "./user";
import edit from "./edit";

Vue.use(Vuex);

const store: StoreOptions<RootState> = {
  state: {
    version: "1.0.0",
    credit: "OpenStreetMap | Geoman-io | CalcRoad - 2019"
  },
  modules: {
    simulation,
    alert,
    user,
    edit
  }
};

export default new Vuex.Store<RootState>(store);
