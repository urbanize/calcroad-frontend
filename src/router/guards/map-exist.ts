import store from "@/store";
import mapServices from "@/services/map";
import { NavigationGuard, Route } from "vue-router";
import { LatLng } from "leaflet";

const mapIsExist: NavigationGuard = (to, from, next) => {
  if (to.params.map === from.params.map) next();
  else
    mapServices
      .get_map(to.params.map)
      .then((resp: any) => {
        next();
      })
      .catch((err: any) => {
        store.dispatch(
          "alert/error",
          "La carte n'existe pas ou l'utilisateur ne peut pas la modifier",
          { root: true }
        );

        if (from && from.name !== "login") {
          next({ name: (from.name || undefined) });
        } else {
          next({ name: "no-map" });
        }
      });
};

export default mapIsExist;
