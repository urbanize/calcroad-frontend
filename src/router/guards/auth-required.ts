import store from "@/store";
import { NavigationGuard } from "vue-router";

/*
    This will check whether the user is authenticated or not.
*/
const authRequired: NavigationGuard = (to, from, next) => {
  if (store.getters["user/isAuthenticated"]) {
    next();
  } else {
    ///////////// prevent edit-*-details path

    let fullPath = to.fullPath;
    let arr = fullPath.split("/");
    // @ts-ignore
    // eslint-disable-next-line no-console
    if (arr.length > 4) arr.pop();
    fullPath = arr.join("/");

    next({ name: "login", query: { redirect_url: fullPath } });
  }
};

export default authRequired;
