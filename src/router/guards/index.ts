import authRequired from "./auth-required";
import mapIsExist from "./map-exist";

export default {
  authRequired: authRequired,
  mapIsExist: mapIsExist
};
