import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import guards from "./guards";
import store from "@/store";

Vue.use(VueRouter);
const lazyLoading = (v: string, f = "views") => (r: any) =>
  import(`@/${f}/${v}.vue`).then(r);

const UserRoutes: RouteConfig[] = [
  { path: "login", name: "login", component: lazyLoading("user/login") },
  {
    path: "logout",
    name: "logout",
    beforeEnter: () => {
      // call the logout func to clean the store.
      store.dispatch("user/logout", { root: true });
    }
  },
  {
    path: "register",
    name: "register",
    component: lazyLoading("user/register")
  },
  {
    path: "change/password:token",
    name: "change-password",
    component: lazyLoading("user/changePassword")
  }
];

const EditRoutes: RouteConfig[] = [
  {
    path: "",
    name: "no-map",
    component: lazyLoading("map/NoMap", "components")
  },
  {
    path: ":map",
    name: "edit",
    component: lazyLoading("map/EditMap", "components"),
    beforeEnter: guards.mapIsExist,
    children: [
      { path: "roads", name: "edit-roads" },
      { path: "roads/:id", name: "edit-roads-detail" },
      { path: "journeys", name: "edit-journeys" },
      { path: "journeys/:id", name: "edit-journeys-detail" },
      //{ path: "events", name: "edit-events" },
      //{ path: "events/:id", name: "edit-events-detail" },
      { path: "users", name: "edit-users" },
      { path: "users/add", name: "edit-users-add" },
      { path: "settings", name: "edit-settings" }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/simulation",
      name: "simulation",
      component: lazyLoading("simulation"),
      beforeEnter: guards.authRequired
    },
    {
      path: "/edit",
      component: lazyLoading("edit"),
      beforeEnter: guards.authRequired,
      children: EditRoutes
    },
    {
      path: "/user",
      name: "user",
      component: lazyLoading("user/index"),
      children: UserRoutes
    },
    { path: "*", redirect: "/edit" }
  ]
});

export default router;
