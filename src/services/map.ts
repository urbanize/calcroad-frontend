import axios, { AxiosRequestConfig } from "axios";
import { Map } from "@/models/maps";

const list_maps = (query: String, scope: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "GET",
    url: "/maps",
    params: {
      title: query,
      scope: scope
    }
  };
  return axios(requestConfig);
};

const get_map = (map_id: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "GET",
    url: "/maps/" + map_id
  };
  return axios(requestConfig);
};

const create_map = (map: Map) => {
  const requestConfig: AxiosRequestConfig = {
    method: "POST",
    url: "/maps",
    headers: { "Content-Type": "application/json" },
    data: map
  };
  return axios(requestConfig);
};

const duplicate_map = (map_id: number, map: Map) => {
  const requestConfig: AxiosRequestConfig = {
    method: "POST",
    url: `/maps/${map_id}/duplicate`,
    headers: { "Content-Type": "application/json" },
    data: map
  };
  return axios(requestConfig);
};

const update_map = (map_id: string, data: any) => {
  const requestConfig: AxiosRequestConfig = {
    method: "PUT",
    url: `/maps/${map_id}`,
    headers: { "Content-Type": "application/json" },
    data: data
  };
  return axios(requestConfig);
};

const delete_map = (map_id: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "DELETE",
    url: `/maps/${map_id}`
  };
  return axios(requestConfig);
};

const get_map_users = (map_id: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "GET",
    url: `/maps/${map_id}/users`
  };
  return axios(requestConfig);
};

const add_map_users = (map_id: string, user: any) => {
  const requestConfig: AxiosRequestConfig = {
    method: "POST",
    url: `/maps/${map_id}/users`,
    data: {
      user_id: user.id,
      read: true,
      write: true
    }
  };
  return axios(requestConfig);
};

const update_map_user = (
  map_id: string,
  user_id: string,
  { read, write, owner }: any
) => {
  const requestConfig: AxiosRequestConfig = {
    method: "PUT",
    url: `/maps/${map_id}/users/${user_id}`,
    data: {
      read: read,
      write: write,
      owner: owner
    }
  };
  return axios(requestConfig);
};

const delete_map_user = (map_id: string, user_id: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "DELETE",
    url: `/maps/${map_id}/users/${user_id}`
  };
  return axios(requestConfig);
};

export default {
  list_maps,
  get_map,
  create_map,
  update_map,
  delete_map,
  duplicate_map,
  get_map_users,
  add_map_users,
  update_map_user,
  delete_map_user
};
