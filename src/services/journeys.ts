import axios, { AxiosRequestConfig } from "axios";

const get_journeys = (map: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "GET",
    url: `/maps/${map}/journeys`
  };
  return axios(requestConfig);
};

const create_journey = (map: string, coords: number[][]) => {
  const requestConfig: AxiosRequestConfig = {
    method: "POST",
    url: `/maps/${map}/journeys`,
    data: {
      name: "New Journey",
      start_point: coords[0],
      end_point: coords[1],
      start_time: 28800 // 8h00
    }
  };
  return axios(requestConfig);
};

const update_journey = (
  map: string,
  id: string,
  name: string,
  coords: number[][],
  start_time: number,
  max_vehicules: number
) => {
  const requestConfig: AxiosRequestConfig = {
    method: "PUT",
    url: `/maps/${map}/journeys/${id}`,
    data: {
      name: name,
      start_point: coords[0],
      end_point: coords[1],
      start_time: start_time,
      max_vehicules: max_vehicules
    }
  };
  return axios(requestConfig);
};

const remove_journey = (map: string, id: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "DELETE",
    url: `/maps/${map}/journeys/${id}`
  };
  return axios(requestConfig);
};

export default { get_journeys, create_journey, update_journey, remove_journey };
