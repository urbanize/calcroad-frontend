import axios, { AxiosRequestConfig } from "axios";
import { UserLogin, UserRegister } from "@/models/user";

const login = (user: UserLogin) => {
  const requestConfig: AxiosRequestConfig = {
    method: "post",
    url: "/auth/token",
    data: {
      email: user.email,
      password: user.password
    }
  };

  return axios(requestConfig);
};

const check_validity = (token: string) => {
  // Authorization is passed in the interceptor
  const requestConfig: AxiosRequestConfig = {
    method: "get",
    url: "users/me"
  };
  return axios(requestConfig);
};

const logout = () => {};

const register = (user: UserRegister) => {
  const requestConfig: AxiosRequestConfig = {
    method: "POST",
    url: "/users/account",
    headers: { "Content-Type": "application/json" },
    data: JSON.stringify(user)
  };

  return axios(requestConfig);
};

const change_password = (token: string, password: string, verify: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "PUT",
    url: "/auth/password",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Basic ${token}`
    },
    data: JSON.stringify({
      password: password,
      verify: verify
    })
  };
  return axios(requestConfig);
};

const update = (user: UserRegister | object) => {
  const requestConfig: AxiosRequestConfig = {
    method: "PUT",
    url: "/users",
    headers: { "Content-Type": "application/json" },
    data: JSON.stringify(user)
  };

  return axios(requestConfig);
};

const get_users = (query: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "GET",
    url: `/users?${query}`
  };
  return axios(requestConfig);
};

export default {
  login,
  logout,
  change_password,
  update,
  register,
  check_validity,
  get_users
};
