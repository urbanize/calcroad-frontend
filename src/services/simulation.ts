import axios, { AxiosRequestConfig } from "axios";
import { FeatureCollection } from "geojson";

const get_simulation = (map_id: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "GET",
    url: `/maps/${map_id}/simulation`
  };
  return axios(requestConfig);
};

const run_simulation = (map_id: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "POST",
    url: `/maps/${map_id}/simulation`
  };
  return axios(requestConfig);
};

const delete_simulation = (map_id: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "DELETE",
    url: `/maps/${map_id}/simulation`
  };
  return axios(requestConfig);
};

const get_report_simulation = (map_id: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "GET",
    url: `/maps/${map_id}/simulation/report`
  };
  return axios(requestConfig);
};

const get_stream_simulation = (
  map_id: string,
  time: number,
  limit: number = 30
) => {
  const requestConfig: AxiosRequestConfig = {
    method: "GET",
    url: `/maps/${map_id}/simulation/stream`,
    params: {
      time: time,
      limit: limit
    }
  };
  return axios(requestConfig);
};

export default {
  get_simulation,
  run_simulation,
  delete_simulation,
  get_report_simulation,
  get_stream_simulation
};
