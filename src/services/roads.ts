import axios, { AxiosRequestConfig } from "axios";
import { LatLngExpression } from "leaflet";

const get_roads = (map: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "GET",
    url: `/maps/${map}/roads`
  };
  return axios(requestConfig);
};

const create_road = (
  map: string,
  coords: LatLngExpression,
  max_speed: number
) => {
  const requestConfig: AxiosRequestConfig = {
    method: "POST",
    url: `/maps/${map}/roads`,
    data: {
      name: "New Road",
      coordinates: coords,
      max_speed: max_speed
    }
  };
  return axios(requestConfig);
};

const remove_road = (map: string, id: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "DELETE",
    url: `/maps/${map}/roads/${id}`
  };
  return axios(requestConfig);
};

const update_road = (
  map: string,
  id: string,
  name: string,
  coords: number[][],
  max_speed: number
) => {
  const requestConfig: AxiosRequestConfig = {
    method: "PUT",
    url: `/maps/${map}/roads/${id}`,
    data: {
      name: name,
      coordinates: coords,
      max_speed: max_speed
    }
  };
  return axios(requestConfig);
};

export default { get_roads, create_road, remove_road, update_road };
