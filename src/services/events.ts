import { LatLngExpression } from "leaflet";

import axios, { AxiosRequestConfig } from "axios";

const get_events = (map: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "GET",
    url: `/maps/${map}/events`
  };
  return axios(requestConfig);
};

const create_event = (map: string, coords: LatLngExpression) => {
  const requestConfig: AxiosRequestConfig = {
    method: "POST",
    url: `/maps/${map}/events`,
    data: {
      name: "New Event",
      coordinates: coords
    }
  };
  return axios(requestConfig);
};

const remove_event = (map: string, id: string) => {
  const requestConfig: AxiosRequestConfig = {
    method: "DELETE",
    url: `/maps/${map}/events/${id}`
  };
  return axios(requestConfig);
};

export default { get_events, create_event, remove_event };
