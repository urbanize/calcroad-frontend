import { EsriProvider } from "leaflet-geosearch";
import { Polyline, Marker } from "leaflet";

export interface GeoSearchControlOptions {
  provider: EsriProvider;
  showMarker: boolean;
  searchLabel: string;
  style: "button" | "bar" | undefined;
}

export interface PmObjCreate {
  layer: Polyline | Marker;
  type: string;
  shape: string;
}
