import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import "./plugins/axios";
import "./plugins/vee-validate";
import "./plugins/leaflet";

import "@/assets/style.scss";

Vue.config.productionTip = false;

/*
wait for session recovery before displaying required view
*/
store.dispatch("user/refreshToken", { root: true }).finally(() => {
  new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
  }).$mount("#app");
});
