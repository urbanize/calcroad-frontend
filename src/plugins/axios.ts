import axios from "axios";
import store from "../store";

axios.defaults.baseURL = process.env.VUE_APP_API_URL;

// Response
axios.interceptors.response.use(
  response => response,
  error => {
    if (error.response.status === 401) {
      store.dispatch("user/logout", { root: true });
      store.dispatch("alert/warning", "Veuillez vous reconnecter.", {
        root: true
      });
    }
    return Promise.reject(error);
  }
);

// Request
axios.interceptors.request.use(
  config => {
    var token = localStorage.getItem("accessToken");

    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    } else {
      delete config.headers["Authorization"];
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);
