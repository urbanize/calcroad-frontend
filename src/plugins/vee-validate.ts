import Vue from "vue";
import {
  required,
  email,
  max,
  confirmed,
  numeric,
  between
} from "vee-validate/dist/rules";
import { extend } from "vee-validate";

extend("required", {
  ...required,
  message: "Ce champ est requis."
});

extend("max", {
  ...max,
  message: "Le champ ne doit pas exéder {length} caractères."
});

extend("email", {
  ...email,
  message: "Ce champ doit être au format E-mail."
});

extend("confirmed", {
  ...confirmed,
  message: "Le mot de passe doit être identique !"
});

extend("numeric", {
  ...numeric,
  message: "Le champ doit contenir uniquement des chiffres !"
});

extend("between", {
  ...between,
  message: "Le champ doit etre entre {min} et {max} !"
});

import { ValidationObserver, ValidationProvider } from "vee-validate";

Vue.component("vee-observer", ValidationObserver);
Vue.component("vee-provider", ValidationProvider);
